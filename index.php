<?php

define("NO_DATA_ON_TABLE", "No data on table");
define("MATCH", "Tables identical");
define("NOT_MATCH", "Tables differ");
define("NOT_FOUND", "Table was not found");
define("EXTRA_TABLE", "Extra table was found");

$FMS_serverName = "212.32.249.1";
$FMS_username = "officero";
$FMS_password = "Of1Gdr!43";
$FMS_dbname = "prosper";

$SAAS_serverName = "212.32.249.2";
$SAAS_username = "officero";
$SAAS_password = "Dev123!";
$SAAS_dbname = "prosper_saas";

$LOCAL_serverName = "127.0.0.1";
$LOCAL_username = "root";
$LOCAL_password = "13131313";
$LOCAL_dbname = "prosper";


//  ############    FMS    ############
$FMS_tablesSchema = getTablesSchema($FMS_serverName, $FMS_username, $FMS_password, $FMS_dbname);

//  ############    LOCAL    ############
$LOCAL_tablesSchema = getTablesSchema($LOCAL_serverName, $LOCAL_username, $LOCAL_password, $LOCAL_dbname);

//  ############    COMPARE    ############
echo createCompareTable($FMS_tablesSchema, $LOCAL_tablesSchema);


/**
 * @param $servername
 * @param $username
 * @param $password
 * @param $dbname
 * @param int $print - use for debug
 * @return array - all tables schemas in given DB
 */
function getTablesSchema($servername, $username, $password, $dbname, $print=0){
    $conn = createConnection($servername, $username, $password, $dbname);
    $tablesNames = getTablesNames($conn);

    $tablesSchema = array();

    if ($tablesNames->num_rows > 0) {
        while ($table = $tablesNames->fetch_assoc()) {
            if ($print){
                print_r($table);
                echo "<br>";
            }
            $tableName = $table['Tables_in_prosper'];
            $tablesSchema[$tableName] = getTableSchema($conn, $tableName);
        }
    }
    $conn->close();
    return $tablesSchema;
}


/**
 * @param $servername
 * @param $username
 * @param $password
 * @param $dbname
 * @return mysqli - connection if success and die otherwise
 */
function createConnection($servername, $username, $password, $dbname){
    $conn = mysqli_connect($servername, $username, $password, $dbname);
    if ($conn->connect_error) {
        die("FMS Connection failed: " . $conn->connect_error);
        exit();
    }
    return $conn;
}


/**
 * @param $conn
 * @return mixed - all tables in given connection
 */
function getTablesNames($conn){
    $showTablesQuery = "SHOW TABLES";
    return $conn->query($showTablesQuery);
}


/**
 * @param $conn
 * @param $tableName
 * @return mixed - table schema for given table name
 */
function getTableSchema($conn, $tableName){
    $schemaQuery = "show create table ".$tableName;
    $tablesSchema = $conn->query($schemaQuery);
    if ($tablesSchema->num_rows > 0) {
        return $tablesSchema->fetch_assoc()['Create Table'];
    } else {
        return NO_DATA_ON_TABLE;
    }
}


/**
 * @param $masterSchemas
 * @param $currentSchemas
 * @return array - holds for each table it's status, and diff (if exists)
 */
function compareTables($masterSchemas, $currentSchemas){
    $compareResults = array();

    foreach ($masterSchemas as $tableName => $tableSchema){
        $diff = array('masterSchema'=>"", 'currentSchema'=>"");
        if (array_key_exists($tableName, $currentSchemas)){
            if ($currentSchemas[$tableName] == $tableSchema){
                $status = MATCH;
            } else {
                $status = NOT_MATCH;
                $diff = getDiff($tableSchema, $currentSchemas[$tableName]);
            }
        } else {
            $status = NOT_FOUND;
        }
        updateTableCompareResult($compareResults, $tableName, $status, $diff['masterSchema'], $diff['currentSchema']);
    }

    $diff = array('masterSchema'=>"", 'currentSchema'=>"");
    foreach ($currentSchemas as $tableName => $tableSchema){
        if (!array_key_exists($tableName, $compareResults)){
            updateTableCompareResult($compareResults, $tableName, EXTRA_TABLE, $diff['masterSchema'], $diff['currentSchema']);
        }
    }
    return $compareResults;
}


/**
 * @param $compareResults - (by reference) add table and details on it
 * @param $tableName
 * @param $status
 * @param $masterSchema - contains data if tables differ
 * @param $currentSchema - contains data if tables differ
 */
function updateTableCompareResult(&$compareResults, $tableName, $status, $masterSchema, $currentSchema){
    $compareResults[$tableName]['status'] = $status;
    $compareResults[$tableName]['masterSchema'] = $masterSchema;
    $compareResults[$tableName]['currentSchema'] = $currentSchema;
}


/**
 * @param $masterSchema
 * @param $currentSchema
 * @param int $print - use for debug
 * @return array - return the diff between the schemas
 */
function getDiff($masterSchema, $currentSchema, $print=0){
    $old = $currentSchema;
    $new =  $masterSchema;

    $from_start = strspn($old ^ $new, "\0");
    $from_end = strspn(strrev($old) ^ strrev($new), "\0");

    $old_end = strlen($old) - $from_end;
    $new_end = strlen($new) - $from_end;

    $start = substr($new, 0, $from_start);
    $end = substr($new, $new_end);
    $new_diff = substr($new, $from_start, $new_end - $from_start);
    $old_diff = substr($old, $from_start, $old_end - $from_start);

    $new = "$start<ins style='background-color:#ccffcc'>$new_diff</ins>$end";
    $old = "$start<del style='background-color:#ffcccc'>$old_diff</del>$end";
    return array('masterSchema'=>$new, 'currentSchema'=>$old);
}


/**
 * @param $masterSchemas
 * @param $currentSchemas
 * @return string - HTML compare table
 */
function createCompareTable($masterSchemas, $currentSchemas){
    $compareResults = compareTables($masterSchemas, $currentSchemas);

    $bgcolorMeaning = "<table>
                            <tr bgcolor='#90ee90'><td>Tables identical</td></tr>
                            <tr bgcolor='#cd5c5c'><td>Tables differ</td></tr>
                            <tr bgcolor='#d3d3d3'><td>Table not Found</td></tr>
                            <tr bgcolor='yellow'><td>Extra table</td></tr>
                        </table>";

    //$tableHeader = "<tr><th>Table Name</th><th>Master Schema</th><th>Current Schema</th></tr>";
    $tableHeader = "<tr><th>Table Name</th><th>Master Schema</th></tr>";
    $tableBody = "";

    foreach ($compareResults as $tableName=>$tableData){
        $status = $tableData['status'];
        $masterSchema = $tableData['masterSchema'];
        $currentSchema = $tableData['currentSchema'];
        switch ($status){
            case MATCH:
                $currentRow = "<td bgcolor = '#90ee90' >";
                break;
            case NOT_MATCH:
                $currentRow = "<td bgcolor = '#cd5c5c' >";
                break;
            case NOT_FOUND:
                $currentRow = "<td bgcolor = '#d3d3d3' >";
                break;
            case EXTRA_TABLE:
                $currentRow = "<td bgcolor = 'yellow' >";
                break;
            default:
                $currentRow = "<td>";

        }
        //$currentRow = "<tr>".$currentRow.$tableName."</td><td>".$masterSchema."</td></tr>";
        $currentRow = "<tr>".$currentRow.$tableName."</td><td>".$masterSchema."</td></tr>";
        $tableBody .= $currentRow;
    }

    $table = "<table border='1'>".$tableHeader.$tableBody."</table>";
    $output = $bgcolorMeaning."<br>".$table;

    return $output;
}



?>