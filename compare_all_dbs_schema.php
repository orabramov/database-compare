<?php
include_once($_SERVER['DOCUMENT_ROOT'] . '/scripts/jobs/compare_db_schema.php');
require_once ($_SERVER['DOCUMENT_ROOT'] . '/utils/SaasEnvsUtils.php');


if($_GET['dbName2'] && $_GET['dbName1'])
{
    $db1 = $_GET['dbName1'];
    $db2 = $_GET['dbName2'];

    $answer = compare_db_schema($db1, $db2);

    echo "<br> ans: <Br><br><pre>";
    print_r($answer);

    die();
}


$prosper_db_names_array = SaasEnvsUtils::getAllSaasEnvironmentsWithProsper();
compare_dbs_array($prosper_db_names_array);

$maca_db_names_array = SaasEnvsUtils::getAllSaasEnvironmentsWithMaca();
compare_dbs_array($maca_db_names_array);

function compare_dbs_array($db_names_array)
{
    //for each 2 DBs, execute the script that compares
    for($i=0; $i<count($db_names_array); $i++)
    {
        $db_name1 = $db_names_array[$i];
        for($j=($i+1); $j<count($db_names_array); $j++)
        {
            $db_name2 = $db_names_array[$j];
            $answer = compare_db_schema($db_name1, $db_name2);

            if($answer['errorCounter']>0)
            {
                echo "<br> ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ";
                echo "<br> compare $db_name1, $db_name2";

                require_once ($_SERVER['DOCUMENT_ROOT'] . '/APIs/slack/slack.php');
                $subject = "@channel DB schema differences - $db_name1 VS $db_name2 found ".$answer['errorCounter']." errors";
                $message = $answer['errorMessage'];
                $channel = "db-schema-alerts";

                echo "*".$subject."* \n ".$message;
                send_slack_message("*".$subject."*  ".$message,$channel);
            }
        }
    }
}
echo "Daniel this is for you";

?>