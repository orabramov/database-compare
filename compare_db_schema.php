<?php


///Users/orabramov/Projects/backoffice/scripts/jobs/compare_all_dbs_schema.php
//                                     scripts/jobs/compare_all_dbs_schema.php

require_once ($_SERVER['DOCUMENT_ROOT'] . '/utils/connectToLocalProsper.php');


function arrayFilter($string) {
    return strpos($string, 'channel_') === false;
}

function compare_db_schema($servername1, $username1, $password1, $dbname1, $servername2, $username2, $password2, $dbname2)
{
    //get the tables and schemas from DBs - array of arrays (key => tables name, value => array(key => field name, value => array of field's data))
    connect_db($servername1, $username1, $password1, $dbname1);
    $tables1 = get_db_tables($db_name1);
    connect_db($servername2, $username2, $password2, $dbname2);
    $tables2 = get_db_tables($db_name2);

    //get the diff tables and the common tables
    $tables_in_db1_not_in_db2 = array_filter(array_keys(array_diff_key($tables1, $tables2)),"arrayFilter");
    $tables_in_db2_not_in_db1 = array_filter(array_keys(array_diff_key($tables2, $tables1)),"arrayFilter");

    $common_tables = array_keys(array_intersect_key($tables1, $tables2));

    $schemas_in_db1_not_in_db2 = array();
    $schemas_in_db2_not_in_db1 = array();

    $field_diff_arr = array();
    //for each common table
    foreach($common_tables as $table_name)
    {
        $table_schema1 = $tables1[$table_name];
        $table_schema2 = $tables2[$table_name];

        //get the table's schema diff and common fields
        $diff = array_keys(array_diff_key($table_schema1, $table_schema2));
        if(!empty($diff))
        {
            $schemas_in_db1_not_in_db2[$table_name] = $diff;
        }
        $diff = array_keys(array_diff_key($table_schema2, $table_schema1));
        if(!empty($diff))
        {
            $schemas_in_db2_not_in_db1[$table_name] = $diff;
        }
        $common_fields = array_keys(array_intersect_key($table_schema1, $table_schema2));

        //for each common field
        foreach($common_fields as $field_name)
        {
            $field_data_arr1 = $table_schema1[$field_name];
            $field_data_arr2 = $table_schema2[$field_name];

            //go over field's data and compare it with the 2 tables
            foreach($field_data_arr1 as $key => $val1)
            {
                $val2 = $field_data_arr2[$key];
                if($val2 != $val1)
                {
                    $field_diff_arr[$table_name][$field_name] .= "\n ** '".$key."' has different value - ".$db_name1.": ".$val1.", ".$db_name2.": ".$val2;
                }
            }
        }
    }

    //build tables diff message
    $tables_diff = "\n";
    if(!empty($tables_in_db1_not_in_db2))
        $tables_diff .= "\n tables in $db_name1 not in $db_name2: ".implode(", ", $tables_in_db1_not_in_db2);
    if(!empty($tables_in_db2_not_in_db1))
        $tables_diff .= "\n tables in $db_name2 not in $db_name1: ".implode(", ", $tables_in_db2_not_in_db1);

    //build schema diff message
    $schema_diff = "\n";
    if(!empty($schemas_in_db1_not_in_db2))
    {
        $schema_diff .= "\n\n schemas in $db_name1 not in $db_name2: ";
        foreach($schemas_in_db1_not_in_db2 as $table_name => $table_schema)
        {
            $schema_diff .= "\n * $table_name: ".implode(", ", $table_schema);
        }
    }
    if(!empty($schemas_in_db2_not_in_db1))
    {
        $schema_diff .= "\n\n schemas in $db_name2 not in $db_name1: ";
        foreach($schemas_in_db2_not_in_db1 as $table_name => $table_schema)
        {
            $schema_diff .= "\n * $table_name: ".implode(", ", $table_schema);
        }
    }

    //build fields diff message
    $diff_msg = "\n";
    foreach($field_diff_arr as $table_name => $diff_arr)
    {
        $diff_msg .= "\n\n table: ".$table_name;
        foreach($diff_arr as $field_name => $msg)
        {
            $diff_msg .= "\n * field: ".$field_name;
            $diff_msg .= $msg;
        }
    }

    $errorCounter = count($tables_in_db1_not_in_db2) + count($tables_in_db2_not_in_db1) + count($schemas_in_db1_not_in_db2) + count($schemas_in_db2_not_in_db1) + count($field_diff_arr);
    return array("errorMessage" => $tables_diff.$schema_diff.$diff_msg, "errorCounter" => $errorCounter);
}


//return array of arrays (key => table name, val => table schema) of the given db
function get_db_tables($db_name)
{
    //ignore tables that starts with channel_
    $query = "SELECT TABLE_NAME FROM information_schema.TABLES WHERE TABLE_SCHEMA = SCHEMA() AND TABLE_NAME NOT LIKE 'channel_%'";
    $result = $GLOBALS['dbManager']->query($query) ;

    $tables = array();
    while ($row = mysqli_fetch_row($result))
    {
        $table_name = $row[0];
        $tables[$table_name] = get_table_schema($table_name);
    }

    return $tables;
}

//return array of schema as inner array (key => Field, val => array of field's data) of the given table
function get_table_schema($table_name)
{
    $query = "DESCRIBE $table_name";
    $result = $GLOBALS['dbManager']->query($query) ;

    $schema = array();
    while ($row = mysqli_fetch_row($result))
    {
        $schema[$row['0']] = array('Type' => $row['1'], 'Null' => $row['2'], 'Key' => $row['3'], 'Default' => $row['4'], 'Extra' => $row['5']);
    }

    ksort($schema);
    return $schema;
}

function connect_db($servername, $username, $password, $dbname)
{
    $GLOBALS['dbManager']->setDb(DB_PROSPER_BY_CLIENT, 0, $db_name);
}
?>